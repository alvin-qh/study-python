# Testing

## Use python unit test toolkit:

### Testing code

A class extends from `unittest.TestCase`

```python
import unittest

class TestFileReadAndWrite(unittest.TestCase):
    
    def test_some_thing(self):
        a = 10
        self.assertEqual(a, 10)
    
    ...
```

### Run testing

#### Run test in same `.py` file

```python
import unittest

class TestFileReadAndWrite(unittest.TestCase):
    
    def test_one(self):
        pass
        
    def test_two(self):
        pass
        
if __name__ == '__main__':
    unittest.main()
```

#### Run all tests in a certain folder

```python
import os
import unittest

TESTS = ['module1', 'module2', 'module3/tests']

def main():
    success = True
    for test_dir in TESTS:
        test_dir = os.path.join(test_dir, '.')
        if os.path.exists(test_dir):
            test_suite = unittest.TestLoader().discover(test_dir, 'test_*.py')
            result = unittest.TextTestRunner(verbosity=1).run(test_suite)
            if len(result.failures) > 0:
                success = False

    return 0 if success else 1
```


## Use py.test

[Document](https://docs.pytest.org/en/latest/contents.html)

### Install py.test

```bash
$ pip install pytest        # install py.test
$ pip install pytest-xdist  # py.test extension, support multi-process testing
$ pip install pytest-cov    # py.test extension, for statistical test coverage
```

### Testing code

- Use `unittest`

```python
import unittest

class TestFileReadAndWrite(unittest.TestCase):
    pass
```

- Any function with `test_` prefix

```python
def test_some_thing():
    n = 10
    assert n == 10 
```

- Any class without `__init__` member function and include member function with `test_` prefix:

```python
class MyTest:
    
    def test_some_thing(self):
        n = 10
        assert n == 10
```

### Run test

- Run all tests in current folder and all sub folders

```bash
$ py.test
```

- Run all tests in multi-process

```bash
$ py.test -n 8
```

- Run all test and statistical test coverage of codes in current folder

```bash
$ py.test --cov=.
```