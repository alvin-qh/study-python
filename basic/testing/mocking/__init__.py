import abc
import os
import re
from abc import ABC, abstractmethod
from datetime import datetime


def get_file_name(path):
    return os.path.basename(path)


class CommandlineError(Exception):
    def __init__(self, msg="Invalid command line"):
        super().__init__(msg)


class Runner(ABC):
    @abstractmethod
    def run(self, params):
        pass

    @property
    @abc.abstractmethod
    def command(self):
        pass


class Terminal:
    _command_split_regexp = re.compile(r'''\s+''')
    _args_regexp = re.compile(
        r'''(\s*[A-Za-z~!@#$%^&*+():;[\]{}_\-.,<>?]+|\s*['"][A-Za-z~!@#$%^&*+():;[\]{}_\s\-.,<>?]+['"])''')

    def __init__(self, *runners):
        self.runners = {c.command: c for c in runners}

    def run(self, command_line: str):
        cmd_parts = self._command_split_regexp.split(command_line, maxsplit=1)
        if not cmd_parts or len(cmd_parts) != 2:
            raise CommandlineError()

        cmd, args = cmd_parts
        if cmd not in self.runners:
            raise CommandlineError("Invalid command")

        args = self._args_regexp.findall(args)

        runner = self.runners[cmd]
        args = list(filter(lambda s: s, map(lambda s: s.strip(' \t\'"'), args)))
        return runner.run(args)


class TimeRunner(Runner):

    def run(self, params):
        t, d = False, False
        pattern = ''

        it = iter(params)
        try:
            while 1:
                p = next(it)
                if p == '--t' or p == '-time':
                    t = True
                elif p == '--d' or p == '-date':
                    d = True
                elif p == '--p' or p == '-pattern':
                    pattern = next(it)
        except StopIteration:
            pass

        time_ = datetime.now()
        if t and not d:
            pattern = pattern or '%H:%M'
            time_ = time_.time()
        elif d and not t:
            pattern = pattern or '%Y-%m-%d'
            time_ = time_.date()

        pattern = pattern or '%Y-%m-%d %H:%M'

        return time_.strftime(pattern)

    @property
    def command(self):
        return 'time'
