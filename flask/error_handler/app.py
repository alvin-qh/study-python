from flask import Flask, render_template

from utils import Assets, templated


def create_app():
    app_ = Flask(__name__, template_folder='templates', static_folder='static')
    app_.jinja_env.globals['assets'] = Assets(app_)
    return app_


app = create_app()


class NothingError(Exception):
    pass


@app.route('/', methods=['GET'])
@templated()
def index():
    return dict()


@app.route('/exception', methods=['GET'])
def exception():
    raise NothingError('Oh shit!!')


@app.errorhandler(404)
def error_404(err):
    return render_template('error-page.html', msg='Page was gone with wind', err=err), 404


@app.errorhandler(NothingError)
def error_exception(err):
    return render_template('error-page.html', msg='Exception was caused', err=err), 500
