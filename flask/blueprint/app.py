from flask import Flask

from utils import Assets


def register_blueprint(app_):
    from .home import bp as home_bp
    from .user import bp as user_bp

    app_.register_blueprint(home_bp, url_prefix='/')
    app_.register_blueprint(user_bp, url_prefix='/user/')


def create_app():
    app_ = Flask(__name__, template_folder='templates', static_folder='static')
    app_.jinja_env.globals['assets'] = Assets(app_)

    register_blueprint(app_)
    return app_


app = create_app()
