from flask import request, Blueprint

from utils import templated

bp = Blueprint('user', __name__)


@bp.route('/', methods=['GET'])
@templated()
def index():
    return dict(user={'name': request.args.get('name') or ''})
