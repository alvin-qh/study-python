from flask import Blueprint

from utils import templated

bp = Blueprint('home', __name__)


@bp.route('/', methods=['GET'])
@templated()
def index():
    return {}
