from flask import Flask
from flask_socketio import SocketIO

from utils.web import Assets, templated


def create_app():
    app_ = Flask(__name__, static_folder='static', template_folder='templates')
    app_.config['DEBUG'] = True
    app_.jinja_env.globals['assets'] = Assets(app_)
    return app_


def create_socketio(app_, debug=True):
    if app_.config.get('SECRET_KEY') is None:
        app_.config['SECRET_KEY'] = 'secret!'
    return SocketIO(app_, engineio_logger=debug)


app = create_app()
socketio = create_socketio(app)


@app.route('/', methods=['GET'])
@templated()
def index():
    return dict()
