import time

from flask import Flask, jsonify

from utils import templated


def create_app():
    return Flask(__name__, static_folder='static', template_folder='templates')


app = create_app()


@app.route('/', methods=['GET'])
def index():
    return '''<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Hello</title>
</head>
<body>
    <h1>Hello World</h1>
    <a href="/template">Next</a>
</body>
</html>
''', 200


# @app.route('/template', methods=['GET'])
# def use_template():
#     return render_template('use_template.html',
#                            data={'title': 'Hello', 'message': 'Hello World'}), 200

@app.route('/template', methods=['GET'])
@templated()
def use_template():
    return dict(data={'title': 'Hello', 'message': 'Hello World'})


@app.route('/json', methods=['GET'])
def use_json():
    tm = time.mktime(time.localtime(time.time()))

    # return jsonify({'time': int(tm)}), 200
    return jsonify(time=int(tm)), 200
