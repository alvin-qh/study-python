from flask import session, redirect, request, g, Flask
from wtforms import (Form, StringField, validators, PasswordField)

from utils import templated, Assets


class User:
    def __init__(self, name, gender, password):
        self.name = name
        self.gender = gender
        self.password = password


USERS = {
    1: User('Alvin', 'M', '123456'),
    2: User('Emma', 'F', '888888')
}


def create_app():
    app_ = Flask(__name__, template_folder='templates', static_folder='static')
    app_.config['SECRET_KEY'] = b'?\xc0\xa9\xfcY\xd7\x9f+\xbe\n\x85\x16\xa0\xd9\xaa\x9fG\x14\x0e\xeb\xf4\x05N\xe3'

    app_.jinja_env.globals['assets'] = Assets(app_)

    def check_login():
        if request.endpoint in {'static', 'login'}:
            return
        if 'user_id' not in session:
            return redirect('/login')

        user = USERS[session['user_id']]
        if not user:
            return redirect('/login')
        g.user = user

    app_.before_request(check_login)
    """
    Or
    @app.before_request
    def check_login():
        if request.endpoint != 'login' and 'user_id' not in session
            return redirect('/login')
    """
    return app_


app = create_app()


class LoginForm(Form):
    account = StringField(
        label='Account',
        validators=[validators.DataRequired(message='Account required'),
                    validators.length(min=2, max=20, message="Account length must between 6 and 20")])

    password = PasswordField(
        label='Password',
        validators=[validators.DataRequired(message='Password required'),
                    validators.length(min=6, max=20, message="Account length must between 6 and 20")])


@app.route('/', methods=['GET', 'POST'])
@templated()
def index():
    return dict(user=g.user)


@app.route('/login', methods=['GET', 'POST'])
@templated()
def login():
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate():
            account = form.account.data
            password = form.password.data
            find_user = next(filter(lambda item: account == item[1].name, USERS.items()), None)
            if find_user:
                if find_user[1].password == password:
                    session['user_id'] = find_user[0]
                    return redirect('/')
                else:
                    form.errors['password'] = ['Invalid password']
            else:
                form.errors['account'] = ['User not exits']

    return dict(form=form), 400 if form.errors else 200


@app.route('/logout', methods=['POST'])
@templated()
def logout():
    user = g.user
    session.clear()
    return dict(user=user)
