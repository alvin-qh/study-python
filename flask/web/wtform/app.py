import wtforms_json
from flask import Flask, request, jsonify
from wtforms import Form, validators, DecimalField, SelectField

from utils import watch_files_for_develop, templated, Assets


def create_app():
    app_ = Flask(__name__, static_folder='static', template_folder='templates')
    app_.jinja_env.globals['assets'] = Assets(app_)
    wtforms_json.init()
    return app_


app = create_app()


class ExpressForm(Form):
    """
    Flask use WT-Forms to validate form
    See also: http://wtforms.readthedocs.io/en/stable/index.html
    """
    a = DecimalField(
        label='a',
        validators=[validators.DataRequired(message='Number a required')])

    b = DecimalField(
        label='b',
        validators=[validators.DataRequired(message='Number b required')])

    op = SelectField(
        label='op',
        choices=(('+', '+'), ('-', '-'), ('*', '*'), ('/', '/')),
        validators=[validators.any_of(values=['+', '-', '*', '/'], message='Operator must on of \'+. -, *, /\'')])


_OP = {
    '+': lambda a, b: a + b,
    '-': lambda a, b: a - b,
    '*': lambda a, b: a * b,
    '/': lambda a, b: a / b
}


@app.route('/', methods=['GET', 'POST'])
@templated()
def index():
    ans, code = '', 200
    form = ExpressForm(request.form)

    if request.method == 'POST':
        if form.validate():
            ans = _OP[form.op.data](float(form.a.data), float(form.b.data))
        else:
            code = 400

    return dict(form=form, ans=ans), code


@app.route('/ajax', methods=['POST'])
def ajax():
    form = ExpressForm.from_json(request.json)

    if not form.validate():
        return jsonify(errors=form.errors), 400

    ans = _OP[form.op.data](float(form.a.data), float(form.b.data))
    return jsonify(ans=ans)


def main():
    app.run(host='0.0.0.0', port=8899, debug=True, extra_files=watch_files_for_develop(app))


if __name__ == '__main__':
    main()
