from flask import request
from wtforms import Form, StringField, validators as v, DateField, RadioField, SelectField


class Page:
    def __init__(self, app):
        self._default_page_size_ = app.config.get('PAGE_SIZE', 5)

    @property
    def page_index(self):
        return request.args.get('p', 1, type=int)

    @property
    def page_size(self):
        return request.args.get('ps', self._default_page_size_, type=int)


class UserForm(Form):
    id_num = StringField(label='Id Num',
                         validators=[v.DataRequired(message='ID Num required')])

    name = StringField(label='Name',
                       validators=[v.DataRequired(message='name required')])

    gender = RadioField(label='Gender',
                        choices=[('M', 'Male'), ('F', 'Female')],
                        default='M',
                        coerce=str,
                        validators=[v.DataRequired(message='gender required'),
                                    v.any_of(['M', 'F'], message='gender must be one of %(values)s')])

    birthday = DateField(label='Birthday')

    group = SelectField(label='Group',
                        coerce=int,
                        validators=[v.DataRequired(message='group required')])


class GroupForm(Form):
    name = StringField(label='Name',
                       validators=[v.DataRequired(message='name required'),
                                   v.length(min=2, max=10, message='name length must between %(min)s and %(max)s')])
