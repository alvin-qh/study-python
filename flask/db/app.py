import pytz
from flask import (Flask, request, redirect, abort)
from sqlalchemy.exc import (IntegrityError)

from utils import (templated, Assets, HttpMethodOverrideMiddleware, watch_files_for_develop)
from .db import (init_app, db, transaction)
from .forms import (Page, UserForm, GroupForm)
from .models import (to_dict, Group, UserGroup, User)


def _tz(app_):
    zone = pytz.timezone(app_.config.get('TIME_ZONE', 'Asia/Shanghai'))

    def time_zone(time):
        return time.replace(tzinfo=pytz.UTC).astimezone(zone)

    return time_zone


def create_app():
    app_ = Flask(__name__, template_folder='templates', static_folder='static')
    app_.wsgi_app = HttpMethodOverrideMiddleware(app_.wsgi_app)

    app_.jinja_env.globals['assets'] = Assets(app_)
    app_.jinja_env.globals['tz'] = _tz(app_)
    app_.config.from_pyfile('conf.py')

    init_app(app_)

    return app_


app = create_app()
page = Page(app)


@app.route('/', methods=['GET'])
@templated()
def index():
    users = User.find_all(page)
    user_ids = [u.id for u in users.items]
    user_groups = {ug.user_id: ug.group.name for ug in UserGroup.find_by_users_with_group(user_ids)}
    return dict(users=users.items, page=dict(page_index=users.page, page_count=users.pages), user_groups=user_groups)


@app.route('/', methods=['POST'])
@templated('user.html')
@transaction()
def create_user():
    form = UserForm(request.form)
    groups_ = Group.find_all()
    form.group.choices = [(g.id, g.name) for g in groups_]
    if not form.validate():
        return dict(form=form), 400

    user = User(id_num=form.id_num.data,
                name=form.name.data,
                gender=form.gender.data,
                birthday=form.birthday.data)

    try:
        user.create()
        group_ = next(filter(lambda g: g.id == form.group.data, groups_))
        UserGroup(user=user, group=group_).create()
        return redirect('/')
    except IntegrityError as err:
        if err.orig.args[0] != 1062:
            raise
        form.id_num.errors = ['\'{}\' already exist, try other id num'.format(form.id_num.data)]
        db.session.rollback()
        return dict(form=form)


@app.route('/new', methods=['GET'])
@templated('user.html')
def new_user():
    form = UserForm()
    form.group.choices = [(g.id, g.name) for g in Group.find_all()]
    return dict(form=form)


@app.route('/<id_>/edit', methods=['GET'])
@templated('user.html')
def edit_user(id_):
    user = User.find_by_id(id_)
    if not user:
        return abort(404)

    form = UserForm(data=to_dict(user))
    form.group.choices = list(map(lambda g: (g.id, g.name), Group.find_all()))
    return dict(id=id_, form=form)


@app.route('/<id_>', methods=['PUT'])
@templated('user.html')
@transaction()
def update_user(id_):
    user = User.find_by_id(id_)
    if not user:
        return abort(404)

    form = UserForm(request.form)
    groups_ = Group.find_all()
    form.group.choices = [(g.id, g.name) for g in groups_]
    if not form.validate():
        return dict(form=form)

    try:
        user.merge(id_num=form.id_num.data,
                   name=form.name.data,
                   gender=form.gender.data,
                   birthday=form.birthday.data)

        UserGroup.delete_by_user(user)

        group_ = next(filter(lambda g: g.id == form.group.data, groups_))
        UserGroup(user=user, group=group_).create()

        return redirect('/')
    except IntegrityError as err:
        if err.orig.args[0] != 1062:
            raise
        form.id_num.errors = ['\'{}\' already exist, try other id num'.format(form.id_num.data)]
        db.session.rollback()
        return dict(form=form)


@app.route('/<id_>', methods=['DELETE'])
@templated()
@transaction()
def delete_user(id_):
    user = User.find_by_id(id_)
    if not user:
        return abort(404)

    UserGroup.delete_by_user(user)
    user.delete()
    return redirect('/')


@app.route('/groups', methods=['GET'])
@templated('group.html')
def groups():
    return dict(back_url=request.args.get('__back', '/'), groups=Group.find_all(), form=GroupForm())


@app.route('/groups', methods=['POST'])
@templated('group.html')
@transaction()
def create_group():
    back_url = request.args.get('__back', '/')
    form = GroupForm(request.form)
    if not form.validate():
        return dict(back_url=back_url, groups=Group.find_all(), form=form), 400

    group_ = Group(name=form.name.data)
    try:
        group_.create()
        return redirect('/groups?__back={}'.format(back_url))
    except IntegrityError as err:
        if err.orig.args[0] != 1062:
            raise
        form.name.errors = ['\'{}\' already exist, try other name'.format(form.name.data)]
        db.session.rollback()
        return dict(back_url=back_url, groups=Group.find_all(), form=form)


@app.route('/groups/<int:id_>', methods=['DELETE'])
@transaction()
def group_delete(id_):
    group_ = Group.find_by_id(id_)
    if not group_:
        abort(404)
    group_.delete()
    return redirect('/groups?__back={}'.format(request.args.get('__back')))


@app.route('/configs', methods=['GET'])
@templated()
def configs():
    configs_ = [item for item in app.config.items() if item[0].startswith('SQLALCHEMY')]
    return dict(configs=configs_)