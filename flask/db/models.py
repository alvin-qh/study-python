from sqlalchemy import (Integer, String, Date, DateTime)
from sqlalchemy.orm import (relationship)

from .db import db


def to_dict(obj):
    return {item[0]: item[1] for item in obj.__dict__.items()
            if not item[0].startswith('_') and not callable(item[1])}


class User(db.Model):
    __tablename__ = 'core_users'
    __table_args__ = {'sqlite_autoincrement': True}

    id = db.Column(Integer, primary_key=True, autoincrement=True)
    id_num = db.Column(String(50), nullable=False)
    name = db.Column(String(50), nullable=False)
    gender = db.Column(String(1), nullable=False)
    birthday = db.Column(Date, nullable=True)
    created_at = db.Column(DateTime, nullable=False, server_default=db.func.now())

    user_groups = db.relationship('UserGroup', backref='user')
    groups = relationship('Group', secondary='core_user_groups', back_populates='users')

    @classmethod
    def find_all(cls, page=None):
        if page:
            return cls.query.paginate(page.page_index, per_page=page.page_size)
        return cls.query.all()

    @classmethod
    def find_by_id(cls, id_):
        return cls.query.filter(cls.id == id_).first()

    def delete(self):
        db.session.delete(self)
        db.session.flush()

    def create(self):
        db.session.add(self)
        db.session.flush()

    def merge(self, **kwargs):
        self.id_num = kwargs.get('id_num', self.id_num)
        self.name = kwargs.get('name', self.name)
        self.gender = kwargs.get('gender', self.gender)
        self.birthday = kwargs.get('birthday', self.birthday)
        db.session.flush()

    def jsonify(self):
        return {
            'id': self.id,
            'id_num': self.id_num,
            'name': self.name,
            'gender': self.gender,
            'birthday': self.birthday,
            'created_at': self.created_at,
            'groups': [g.jsonify() for g in self.groups]
        }


class Group(db.Model):
    __tablename__ = 'core_groups'
    __table_args__ = {'sqlite_autoincrement': True}

    id = db.Column(Integer, primary_key=True, autoincrement=True)
    name = db.Column(String(50), nullable=False)
    created_at = db.Column(DateTime, nullable=False, server_default=db.func.now())

    user_groups = db.relationship('UserGroup', backref='group')
    users = relationship('User', secondary='core_user_groups', back_populates='groups')

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_id(cls, id_):
        return cls.query.filter(cls.id == id_).first()

    def delete(self):
        db.session.delete(self)
        db.session.flush()

    def create(self):
        db.session.add(self)
        db.session.flush()

    def jsonify(self):
        return {
            'id': self.id,
            'name': self.name,
            'created_at': self.created_at
        }


class UserGroup(db.Model):
    __tablename__ = 'core_user_groups'
    __table_args__ = {'sqlite_autoincrement': True}

    id = db.Column(Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(Integer, db.ForeignKey('core_users.id'), nullable=False)
    group_id = db.Column(Integer, db.ForeignKey('core_groups.id'), nullable=False)
    created_at = db.Column(DateTime, nullable=False, server_default=db.func.now())

    # user - backref by User.user_group
    # group - backref by Group.user_group

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_users_with_group(cls, user_ids):
        return cls.query.filter(cls.user_id.in_(user_ids)) \
            .join(User, db.and_(User.id == cls.user_id)) \
            .all()

    @classmethod
    def delete_by_user(cls, user):
        cls.query.filter(cls.user_id == user.id).delete()

    def create(self):
        db.session.add(self)
        db.session.flush()

    def delete(self):
        db.session.delete(self)
        db.session.flush()


def clear_tables():
    for table in [User, Group, UserGroup]:
        db.session.execute('DELETE FROM {}'.format(table.__tablename__))
        db.session.commit()
