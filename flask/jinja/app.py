from flask import render_template, request, jsonify, Flask

from utils import templated, Assets


def create_app(context_path='/'):
    app_ = Flask(__name__, template_folder='templates', static_folder='static')

    app_.jinja_env.globals['assets'] = Assets(app_)
    app_.jinja_env.globals['url'] = lambda url: context_path + url

    return app_


app = create_app()


@app.route('/', methods=['GET'])
@templated()
def index():
    return render_template('home/index.html'), 200


@app.route('/api/search', methods=['GET'])
def search():
    key = request.args.get('key')
    if not key:
        return jsonify(message='Invalid key word'), 400

    return jsonify(results=[{
        'title': 'Welcome | Jinja2 (The Python Template Engine)',
        'description': 'Jinja is Beautiful {% extends "layout.html" %} {% block body %} <ul> {% for user in users %} '
                       '<li><a href="{{ user.url }}">{{ user...',
        'url': 'http://jinja.pocoo.org'
    }]), 200
