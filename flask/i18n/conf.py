SECRET_KEY = b'?\xc0\xa9\xfcY\xd7\x9f+\xbe\n\x85\x16\xa0\xd9\xaa\x9fG\x14\x0e\xeb\xf4\x05N\xe3'

BABEL_TRANSLATION_DIRECTORIES = 'message'
BABEL_DEFAULT_LOCALE = 'zh_Hans_CN'
BABEL_DEFAULT_TIMEZONE = 'UTC'
BABEL_SUPPORT_LOCALES = ['en', 'en_US', 'zh', 'zh_CN', 'zh_TW']
