from flask import Flask, request, session
from flask_babel import Babel, refresh, lazy_gettext as _

from utils import templated, Assets


def babel_config(app_):
    babel = Babel(app_)

    @babel.localeselector
    def get_locale():
        lang = session.get('lang')
        if lang:
            return lang
        return request.accept_languages.best_match(app_.config['BABEL_SUPPORT_LOCALES'], 'zh_CN')

    @babel.timezoneselector
    def get_timezone():
        zone = session.get('time_zone')
        if zone:
            return zone
        return 'UTC'


def create_app():
    app_ = Flask(__name__, static_folder='static', template_folder='templates')
    app_.jinja_env.globals['assets'] = Assets(app_)

    app_.config.from_pyfile('conf.py')
    babel_config(app_)

    return app_


app = create_app()


@app.route('/', methods=['GET'])
@templated()
def index():
    lang = request.args.get('lang')
    if lang:
        session['lang'] = lang
        refresh()
    else:
        lang = session.get('lang', 'zh_CN')

    langs = {'zh_CN': _('zh_CN'), 'en_US': _('en_US')}

    return dict(current_lang=langs[lang])
