import glob
import os
import pkgutil
import sys

from flask import Flask


def _is_file_valid(file):
    file = os.path.basename(file)
    return file != '.' and not (file.startswith('__') and file.endswith('__'))


def list_all_files(paths, patterns, recursive=False):
    if not isinstance(paths, (list, tuple, set)):
        paths = [paths]

    if not isinstance(patterns, (list, tuple, set)):
        patterns = [patterns]

    files = set()
    for path in paths:
        for pattern in patterns:
            pattern = os.path.join(path, pattern)
            files.update({f for f in glob.glob(pattern, recursive=recursive) if _is_file_valid(f)})
    return list(files)


def get_module_path(module_name):
    loader = pkgutil.get_loader(module_name)

    if hasattr(loader, 'get_filename'):
        filename = loader.get_filename(module_name)
    elif hasattr(loader, 'archive'):
        filename = loader.archive
    else:
        __import__(module_name)
        filename = sys.modules[module_name].__file__

    return os.path.abspath(os.path.dirname(filename))


def watch_files_for_develop(app: Flask, types=('py', 'html', 'js', 'jsx', 'ts', 'vue', 'less', 'css')):
    if not app.debug:
        return None

    package_path = get_module_path(app.import_name)
    paths = (package_path, app.template_folder, app.static_folder)
    patterns = {os.path.join('**', '*.{}'.format(t)) for t in types}
    return list_all_files(paths, patterns, recursive=True)
