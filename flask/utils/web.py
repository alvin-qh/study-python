import os
from functools import wraps
from time import time

import xxhash
from flask import render_template, request, json
from werkzeug.urls import url_decode


def templated(template=None):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            template_name = template or request.endpoint.replace('.', '/') + '.html'

            ctx, code = f(*args, **kwargs), 200
            if type(ctx) == tuple:
                ctx, code = ctx

            if ctx is None:
                ctx = {}
            elif not isinstance(ctx, dict):
                return ctx

            return render_template(template_name, **ctx), code

        return decorated_function

    return decorator


class Assets:
    def __init__(self, app):
        self._static_folder = app.static_folder
        self._static_url_path = app.static_url_path
        self._asset_hash_cache = {}
        self._asset_file_cache = {}
        self._debug = app.debug

        manifest_file = os.path.join(app.static_folder, 'manifest.json')
        if os.path.exists(manifest_file):
            with open(manifest_file, 'r') as f:
                self._asset_file_cache = json.loads(f.read())

    def image(self, key, fixed=False):
        return self._asset('images/' + key, fixed)

    def css(self, key, fixed=False):
        return self._asset('css/' + key, fixed)

    def js(self, key, fixed=False):
        return self._asset('js/' + key, fixed)

    def _calculate_asset_hash(self, asset_file):
        hash_ = self._asset_hash_cache.get(asset_file)
        if not hash_:
            file = os.path.join(self._static_folder, asset_file)
            if os.path.isfile(file):
                with open(file, 'rb') as f:
                    data = f.read()
                    hash_ = xxhash.xxh64(data).hexdigest()
                    self._asset_hash_cache[asset_file] = hash_

        return hash_

    def _asset(self, filename, fixed):
        if self._debug and not fixed:
            return '{}/{}'.format(self._static_url_path, filename + ('?__v={}'.format(time())))

        if filename in self._asset_file_cache:
            return '{}/{}'.format(self._static_url_path, self._asset_file_cache[filename])

        hash_ = self._calculate_asset_hash(filename)
        return '{}/{}'.format(self._static_url_path, filename + (('?__v=' + hash_) if hash_ else ''))


class HttpMethodOverrideMiddleware:
    allowed_methods = frozenset(['GET',
                                 'HEAD',
                                 'POST',
                                 'DELETE',
                                 'PUT',
                                 'PATCH',
                                 'OPTIONS'])
    body_less_methods = frozenset(['GET', 'HEAD', 'OPTIONS', 'DELETE'])

    def __init__(self, wsgi_app, name='__method'):
        self._app = wsgi_app
        self._name = name

    def __call__(self, environ, start_response):
        if environ.get('REQUEST_METHOD', '') == 'POST':
            qs = environ.get('QUERY_STRING', '')
            if self._name in qs:
                method = url_decode(qs).get(self._name)

                if method in self.allowed_methods:
                    environ['REQUEST_METHOD'] = method

                if method in self.body_less_methods:
                    environ['CONTENT_LENGTH'] = '0'

        return self._app(environ, start_response)
