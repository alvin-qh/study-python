from flask import Flask, request, redirect

from utils import Assets, HttpMethodOverrideMiddleware, templated


def create_app():
    app_ = Flask(__name__, static_folder='static', template_folder='templates')
    app_.wsgi_app = HttpMethodOverrideMiddleware(app_.wsgi_app)

    app_.jinja_env.globals['assets'] = Assets(app_)
    return app_


app = create_app()

NAMES = set()


def _search_result():
    kwd = request.args.get('kwd', '')
    if kwd:
        results = list(filter(lambda n: n.find(kwd) >= 0, NAMES))
    else:
        results = list(NAMES)
    return dict(name=kwd, results=results)


def _add_name():
    name = request.form.get('name')
    if name:
        NAMES.add(name)
    return redirect('/')


def _change_name():
    old_value = request.form.get('old_value')
    if old_value in NAMES:
        NAMES.remove(old_value)

    new_value = request.form.get('new_value')
    NAMES.add(new_value)
    return redirect('/')


def _delete_name():
    value = request.args.get('value')
    if value in NAMES:
        NAMES.remove(value)

    return redirect('/')


@app.route('/', methods=['GET', 'POST', 'PUT', 'DELETE'])
@templated()
def index():
    return {
        'GET': _search_result,
        'POST': _add_name,
        'PUT': _change_name,
        'DELETE': _delete_name
    }[request.method]()
