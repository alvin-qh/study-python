import os

from flask import Flask

from utils import templated, Assets


def create_app(env=''):
    app_ = Flask(__name__, template_folder='templates', static_folder='static')

    app_.jinja_env.globals['assets'] = Assets(app_)

    if env:
        app_.config.from_pyfile('conf_{}.py'.format(env))
    else:
        app_.config.from_pyfile('conf.py'.format(env))
    return app_


app = create_app(os.environ.get('ENV'))


@app.route('/', methods=['GET'])
@templated()
def index():
    return app.config
