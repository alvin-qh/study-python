import click
from click import Choice, IntRange


@click.group()
def cli():
    pass


LANGUAGE_SET = ['en', 'zh']


@click.command('demo')
@click.option('-n', '--name', 'name',
              type=str, required=True,
              help='Name of people')
@click.option('-l', '--lang', 'lang',
              type=Choice(LANGUAGE_SET), required=False,
              help='Language in {} and "en" is default'.format(LANGUAGE_SET),
              default='en')
@click.option('-c', '--count', 'count',
              type=IntRange(1, 20), required=False,
              help='Number between 1 and 20, default is 10',
              default=10)
def demo(name: str, lang: str, count: int):
    print('* name is "{}"'.format(name))
    print('* language is "{}"'.format(lang))
    print('* count is "{}"'.format(count))


cli.add_command(demo)

if __name__ == '__main__':
    cli()
