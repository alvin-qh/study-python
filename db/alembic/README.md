# Alembic DB Migration

## Setup

### Install package

```bash
$ pip install PyMySql
$ pip install alembic
$ pip install SQLAlchemy
```

### Create Project

```bash
$ alembic init <Project Dir>
```

The following files and directories would be created in target folder

```
versions            <dir>
alembic.ini         <file>
env.py              <file>
script.py.mako      <file>
```

Edit `alembic.ini`, change `script_location` option, make sure it point to `versions` folder by relative

Edit `alembic.ini`, set db connection:

```ini
sqlalchemy.url = mysql+pymysql://root:@localhost/xxxxx
```

Use special `.ini` file:
```bash
$ alembic -c test.ini upgrade head
```

## Upgrade and Downgrade

Upgrade/Downgrade to certain version:

```bash
$ alembic upgrade <Revision ID>
$ alembic downgrade <Revision ID>
```

Upgrade to newest version:

```bash
$ alembic upgrade head
```

Downgrade to oldest version:

```bash
$ alembic downgrade base
```