import json

from sqlalchemy import (create_engine, Column, Integer, String, Date, DateTime, func, ForeignKey)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.pool import QueuePool

# 创建数据库连接引擎
engine = create_engine('sqlite:///:memory:', echo=False, pool_size=20, max_overflow=0, poolclass=QueuePool)

# 创建 Session
Session = sessionmaker(bind=engine)

# 创建模型基类
Base = declarative_base()


class ObjectEncoder(json.JSONEncoder):
    """
    Json 序列化扩展类
    """

    def default(self, obj):
        from datetime import date, datetime

        if isinstance(obj, date):
            return obj.isoformat()
        elif isinstance(obj, datetime):
            return obj.isoformat()
        return None


class User(Base):
    __tablename__ = 'core_users'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    id_num = Column(String(length=50), nullable=False)
    name = Column(String(length=50), nullable=False)
    gender = Column(String(length=1), nullable=False)
    birthday = Column(Date, nullable=True)
    created_at = Column(DateTime, nullable=False, server_default=func.now())

    # 连接到 UserGroup 类，外键 `core_users.id`, 在 `UserGroup` 对象添加 user 字段
    user_group = relationship('UserGroup', backref='user')

    # 以 `core_user_groups` 为中间表，连接到 Group 类, 在 `Group` 对象添加 users 字段
    groups = relationship('Group', secondary='core_user_groups', back_populates='users')

    def jsonify(self):
        return {
            'id': self.id,
            'id_num': self.id_num,
            'name': self.name,
            'gender': self.gender,
            'birthday': self.birthday,
            'created_at': self.created_at
        }

    def __str__(self):
        return json.dumps(self.jsonify(), cls=ObjectEncoder, indent=2)


class Group(Base):
    __tablename__ = 'core_groups'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(length=50), nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())

    # 连接到 UserGroup 类，外键 `core_users.id`, 在 `UserGroup` 对象添加 group 字段
    user_group = relationship('UserGroup', backref='group')

    # 以 `core_user_groups` 为中间表，连接到 User 类, 在 `User` 对象添加 groups 字段
    users = relationship('User', secondary='core_user_groups', back_populates='groups')

    def jsonify(self):
        return {
            'id': self.id,
            'name': self.name,
            'created_at': self.created_at
        }

    def __str__(self):
        return json.dumps(self.jsonify(), cls=ObjectEncoder, indent=2)


class UserGroup(Base):
    __tablename__ = 'core_user_groups'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('core_users.id'), nullable=False)
    group_id = Column(Integer, ForeignKey('core_groups.id'), nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())

    # user - backref by User.user_group
    # user = relationship('User', back_populates='user_group')

    # group - backref by Group.user_group
    # group = relationship('Group', back_populates='user_group')

    def jsonify(self):
        return {
            'id': self.id,
            'user': self.user.jsonify(),
            'group': self.group.jsonify(),
            'created_at': self.created_at
        }

    def __str__(self):
        return json.dumps(self.jsonify(), cls=ObjectEncoder, indent=2)
