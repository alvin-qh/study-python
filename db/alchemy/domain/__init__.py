from .models import Session, User, Group, UserGroup


def initialize_tables():
    from .models import engine

    for table in [User, Group, UserGroup]:
        try:
            table.__table__.create(engine)
        except:
            pass

        sess = Session()
        sess.execute('DELETE FROM {}'.format(table.__tablename__))
        sess.commit()
